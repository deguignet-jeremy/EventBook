package com.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import com.service.EventService;
import com.tools.Tools;

/**
 * Servlet implementation class DisplayPieServlet
 */
@WebServlet("/DisplayPieServlet")
public class DisplayPieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DisplayPieServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DefaultPieDataset pieDataset = new DefaultPieDataset();
		try {
			Integer eventId = Tools.getIntFromString(request, "id");
			if (eventId == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
				return;
			}
			pieDataset.setValue("Pending", eventService.calculMeanStatusForEvent(eventId, "pending"));
			pieDataset.setValue("Confirmed", eventService.calculMeanStatusForEvent(eventId, "confirm"));
			pieDataset.setValue("Refused", eventService.calculMeanStatusForEvent(eventId, "refuse"));
			JFreeChart pieChart = ChartFactory.createPieChart("Participant Status", pieDataset, true, true, true);
			OutputStream out = response.getOutputStream();
			response.setContentType("image/png");
			ChartUtilities.writeChartAsPNG(out, pieChart, 500, 400);
		} catch (IllegalStateException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}
}