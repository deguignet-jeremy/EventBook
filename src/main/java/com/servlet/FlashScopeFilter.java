package com.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class Logging
 */
@WebFilter("/*")
public class FlashScopeFilter implements Filter {

	private static final String FLASH_SESSION_KEY = "FLASH_SESSION_KEY";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (request instanceof HttpServletRequest) {
			checkSession(request);
		}

		chain.doFilter(request, response);

		if (request instanceof HttpServletRequest) {
			subFilter(request);
		}
	}

	@Override
	public void destroy() {
		// void
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		// void
	}

	private void checkSession(ServletRequest request) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession(false);
		if (session != null) {
			checkParam(session, request);
		}
	}

	@SuppressWarnings("unchecked")
	private void checkParam(HttpSession session, ServletRequest request) {
		Map<String, Object> flashParams = (Map<String, Object>) session.getAttribute(FLASH_SESSION_KEY);
		if (flashParams != null) {
			for (Map.Entry<String, Object> flashEntry : flashParams.entrySet()) {
				request.setAttribute(flashEntry.getKey(), flashEntry.getValue());
			}
			session.removeAttribute(FLASH_SESSION_KEY);
		}
	}

	private void subFilter(ServletRequest request) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Map<String, Object> flashParams = new HashMap<>();
		Enumeration<String> e = httpRequest.getAttributeNames();

		subFilterLoop(e, request, flashParams);

		if (flashParams.size() > 0) {
			HttpSession session = httpRequest.getSession(false);
			session.setAttribute(FLASH_SESSION_KEY, flashParams);
		}
	}

	private void subFilterLoop(Enumeration<String> e, ServletRequest request, Map<String, Object> flashParams) {
		while (e.hasMoreElements()) {
			String paramName = e.nextElement();
			if (paramName.startsWith("flash.")) {
				Object value = request.getAttribute(paramName);
				paramName = paramName.substring(6, paramName.length());
				flashParams.put(paramName, value);
			}
		}
	}
}
