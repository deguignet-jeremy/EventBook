package com.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.service.EventService;
import com.tools.Tools;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/ImageServlet")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(EventDetailServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer eventId = Tools.getIntFromString(request, "id");
		if (eventId == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
			return;
		}
		Event event = eventService.getEvent(eventId);
		File file = new File(event.getPathFile());
		String filename = event.getPathFile().substring(23);
		if (filename != null) {
			response.setHeader("Content-Type", getServletContext().getMimeType(filename));
			response.setHeader("Content-Length", String.valueOf(file.length()));
			try {
			Files.copy(file.toPath(), response.getOutputStream());			
			} catch (IOException | IllegalStateException e) {
				logger.catching(e);			
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");	
			}			
		}
	}
}
