package com.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.service.EventService;

/**
 * Servlet implementation class ConfirmServlet
 */
@WebServlet("/ConfirmServlet")
public class ConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String EVENT = "event";
	private static final String HASHCODE = "hashCode";

	private static final Logger logger = LogManager.getLogger(ConfirmServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String hashCode = request.getParameter("id");
			if (hashCode == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalide selected choice");
				return;
			}
			Event event = eventService.getEventFromHashcode(hashCode);
			request.setAttribute(EVENT, event);
			request.setAttribute(HASHCODE, hashCode);
			request.getRequestDispatcher("/WEB-INF/confirm/confirmForm.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String hashCode = request.getParameter(HASHCODE);
			String status = request.getParameter("eventPresence");
			if (hashCode == null || status == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalide selected choice");
				return;
			}
			eventService.updateUserPresenceStatus(hashCode, status);
			request.setAttribute("flash.message", "Your participation has been updated with success.");
			response.sendRedirect("EventListServlet");
		} catch (IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}
}
