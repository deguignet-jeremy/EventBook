package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.service.EventService;

/**
 * Servlet implementation class EventListServlet
 */
@WebServlet("/EventListServlet")
public class EventListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LogManager.getLogger(EventListServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			List<Event> events = eventService.getAllEvents();
			request.setAttribute("events", events);
			request.getRequestDispatcher("/WEB-INF/eventList/eventList.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}
}
