package com.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EventService;

/**
 * Servlet implementation class CreateEventServlet
 */
@WebServlet("/CreateEventServlet")
@MultipartConfig
public class CreateEventServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String EVENTJSPPATH = "/WEB-INF/create/createForm.jsp";

	private static final Logger logger = LogManager.getLogger(CreateEventServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			boolean isUsernameSessionFound = request.getSession(false).getAttribute("username") != null;
			if (!isUsernameSessionFound) {
				response.sendRedirect("Login");
				return;
			}
			request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Map<String, String> errors = isEmptyRequestParametersTitleOrDescription(request);
			if (!errors.isEmpty()) {
				request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
				return;
			}
			String pathFile = saveFile(request);
			eventService.createEvent(pathFile, request);
			request.setAttribute("flash.message", "Event has been created");
			response.sendRedirect("EventListServlet");
		} catch (IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

	private Map<String, String> isEmptyRequestParametersTitleOrDescription(HttpServletRequest request) {
		String title = request.getParameter(TITLE);
		String description = request.getParameter(DESCRIPTION);

		Map<String, String> errors = new HashMap<>();

		if (title == null || title.isEmpty()) {
			errors.put(TITLE, "Name must be specified");
		}

		if (description == null || description.isEmpty()) {
			errors.put(DESCRIPTION, "Description must be specified");
		}

		if (!errors.isEmpty()) {
			request.setAttribute(TITLE, title);
			request.setAttribute(DESCRIPTION, description);
			request.setAttribute("errors", errors);
		}
		return errors;
	}

	private String saveFile(HttpServletRequest request) {
		Part filePart;
		File outputFile = null;

		try {
			filePart = request.getPart("file");
			String fileName = filePart.getSubmittedFileName();
			InputStream is = filePart.getInputStream();

			outputFile = new File("/formationJava/uploads", fileName);
			Files.copy(is, outputFile.toPath());
		} catch (IOException | ServletException e) {
			logger.catching(e);
		}
		if (outputFile != null) {
			return outputFile.toString();
		} else {
			return null;
		}
	}
}