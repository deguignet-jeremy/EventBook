package com.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EventService;

/**
 * Servlet implementation class LoggingServlet
 */
@WebServlet("/Login")
public class LoggingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LogManager.getLogger(EventDetailServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.getRequestDispatcher("/WEB-INF/login/login.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			HttpSession session = request.getSession();

			String username = request.getParameter("username");
			String password = request.getParameter("password");

			if (username.isEmpty() || password.isEmpty()) {
				request.getRequestDispatcher("/WEB-INF/login/login.jsp").forward(request, response);
				return;
			}

			boolean isExist = eventService.isAuthorExists(username);
			if (!isExist) {
				response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Authentification error, wrong username");
				return;
			}
			boolean isCorrect = eventService.checkPassword(username, password);
			if (!isCorrect) {
				response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Authentification error, wrong password");
				return;
			}

			session.setAttribute("username", username);
			request.setAttribute("flash.message", "Welcome " + username);
			response.sendRedirect("EventListServlet");

		} catch (IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

}
