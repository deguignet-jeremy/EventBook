package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.service.EventService;
import com.tools.Tools;
import com.tools.UserStatusForEvent;

/**
 * Servlet implementation class EventDetailServlet
 */
@WebServlet("/EventDetailServlet")
public class EventDetailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String EVENTID = "eventId";
	private static final String EMAIL = "email";
	private static final String EVENT = "event";
	private static final String EVENTJSPPATH = "/WEB-INF/eventList/eventDetail.jsp";

	private static final Logger logger = LogManager.getLogger(EventDetailServlet.class);

	@Inject
	private EventService eventService;

	@Inject
	javax.enterprise.event.Event<Integer> sendEmailForTheEvent;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			boolean isUsernameSessionFound = request.getSession(false).getAttribute("username") != null;
			if (!isUsernameSessionFound) {
				response.sendRedirect("Login");
				return;
			}

			HttpSession session = request.getSession(false);
			Integer eventId = Tools.getIntFromString(request, "id");
			if (eventId == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
				return;
			}
			String username = session.getAttribute("username").toString();
			boolean isUsernameMatchWithEventAuthor = eventService.checkUsernameWithEventAuthor(username, eventId);
			if (!isUsernameMatchWithEventAuthor) {
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Forbidden");
				return;
			}

			Event event = eventService.getEvent(eventId);
			request.setAttribute(EVENTID, eventId);
			request.setAttribute(EVENT, event);

			Map<String, List<String>> mapUserStatus = eventService.getUsersWithStatusParticipatingToEvent(eventId);

			request.setAttribute("pendingUsers", mapUserStatus.get(UserStatusForEvent.PENDING.toString().toLowerCase()));
			request.setAttribute("confirmUsers", mapUserStatus.get(UserStatusForEvent.CONFIRM.toString().toLowerCase()));
			request.setAttribute("refuseUsers", mapUserStatus.get(UserStatusForEvent.REFUSE.toString().toLowerCase()));

			request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Integer eventId = Tools.getIntFromString(request, EVENTID);
			if (eventId == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
				return;
			}
			Map<String, String> errors = isRequestParametersNullOrEmpty(request);
			if (!errors.isEmpty()) {
				request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
				return;
			}
			List<String> emailToSend = getEmailsToSend(request);

			int pendingInvitations = eventService.insertToDbUsersAndEvents(emailToSend, eventId);

			if (pendingInvitations != 0) {
				sendEmailForTheEvent.fire(eventId);
			}

			fillRequest(request, eventId);
			request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);

		} catch (IOException | IllegalStateException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

	private Map<String, String> isRequestParametersNullOrEmpty(HttpServletRequest request) throws IOException {

		int eventId = Tools.getIntFromString(request, EVENTID);

		Map<String, String> errors = new HashMap<>();

		if (request.getParameter(EMAIL) == null || request.getParameter(EMAIL).isEmpty()) {
			errors.put(EMAIL, "Email must be specified");
		}

		if (!errors.isEmpty()) {
			Event event = eventService.getEvent(eventId);
			request.setAttribute(EVENT, event);
			request.setAttribute(EMAIL, request.getParameter(EMAIL));
			request.setAttribute("errors", errors);
			request.setAttribute(EVENTID, eventId);
		}
		return errors;
	}

	private List<String> getEmailsToSend(HttpServletRequest request) {
		List<String> emailToSend = new ArrayList<>();
		String email = request.getParameter(EMAIL);
		String[] emails = email.split(";");
		for (String string : emails) {
			emailToSend.add(string);
		}
		return emailToSend;
	}

	private void fillRequest(HttpServletRequest request, int eventId) {

		Map<String, List<String>> mapUserStatus = eventService.getUsersWithStatusParticipatingToEvent(eventId);
		request.setAttribute("pendingUsers", mapUserStatus.get(UserStatusForEvent.PENDING.toString().toLowerCase()));
		request.setAttribute("confirmUsers", mapUserStatus.get(UserStatusForEvent.CONFIRM.toString().toLowerCase()));
		request.setAttribute("refuseUsers", mapUserStatus.get(UserStatusForEvent.REFUSE.toString().toLowerCase()));
		Event event = eventService.getEvent(eventId);
		request.setAttribute(EVENT, event);
		request.setAttribute(EMAIL, request.getParameter(EMAIL));
		request.setAttribute(EVENTID, eventId);
	}
}
