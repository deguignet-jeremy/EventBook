package com.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EventService;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String EVENTJSPPATH = "/WEB-INF/register/register.jsp";

	private static final Logger logger = LogManager.getLogger(EventStatServlet.class);

	@Inject
	private EventService eventService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Map<String, String> errors = isRequestParametersNullOrEmpty(request);
			if (!errors.isEmpty()) {
				request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
				return;
			}

			eventService.createAuthor(request);
			request.setAttribute("flash.message", "You have been registered. Please try logging.");
			response.sendRedirect("Login");
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}
	
	private Map<String, String> isRequestParametersNullOrEmpty(HttpServletRequest request) throws IOException {

		String username = request.getParameter("username");
		String passwordFirst = request.getParameter("password");
		String passwordSecond = request.getParameter("confirm password");
		
		Map<String, String> errors = new HashMap<>();

		if (username == null || username.isEmpty()) {
			errors.put("username", "Username must be specified");
		}
		if (passwordFirst == null || passwordFirst.isEmpty()) {
			errors.put("passwordFirst", "Password must be specified");
		}
		if (passwordSecond == null || passwordSecond.isEmpty()) {
			errors.put("passwordSecond", "Password must be specified");
		}

		if (passwordFirst != passwordSecond) {
			errors.put("checkPassword", "Passwords must be identic");
		}

		if (!errors.isEmpty()) {	
			request.setAttribute("errors", errors);
			request.setAttribute("username", username);
		}
		return errors;
	}

}
