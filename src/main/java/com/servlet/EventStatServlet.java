package com.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.service.EventService;
import com.tools.Tools;

/**
 * Servlet implementation class EventStatServlet
 */
@WebServlet("/EventStatServlet")
public class EventStatServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String EVENTID = "eventId";
	private static final String EVENT = "event";
	private static final String EVENTJSPPATH = "/WEB-INF/eventList/eventStat.jsp";

	private static final Logger logger = LogManager.getLogger(EventStatServlet.class);

	@Inject
	private EventService eventService;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			boolean isUsernameSessionFound = request.getSession(false).getAttribute("username") != null;
			if (!isUsernameSessionFound) {
				response.sendRedirect("Login");
				return;
			}
			Integer eventId = Tools.getIntFromString(request, "id");
			if (eventId == null) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Page not found");
				return;
			}
			Event event = eventService.getEvent(eventId);
			request.setAttribute(EVENTID, eventId);
			request.setAttribute(EVENT, event);
			request.getRequestDispatcher(EVENTJSPPATH).forward(request, response);
		} catch (ServletException | IOException e) {
			logger.catching(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}
}