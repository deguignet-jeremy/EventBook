package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_participate_to_event")
public class UserParticipatingToEvent {

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int id;
		
		@ManyToOne
		private Event event;
		
		@ManyToOne
		private User user;
		
		private String status;
		
		@Column(name="hashEmailUser_IdEvent")
		private String hashCode;

		public UserParticipatingToEvent() {
			this(0,null,null,null,null);
		}

		public UserParticipatingToEvent(int id, Event event, User user, String status, String hashCode) {
			super();
			this.id = id;
			this.event = event;
			this.user = user;
			this.status = status;
			this.hashCode = hashCode;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}



		public Event getEvent() {
			return event;
		}

		public void setEvent(Event event) {
			this.event = event;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getHashCode() {
			return hashCode;
		}

		public void setHashCode(String hashCode) {
			this.hashCode = hashCode;
		}
}
