package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="events")
public class Event {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="name")
	private String title;
	
	private String description;
	private String pathFile;
	
	@ManyToOne
	private Author author;
	
	public Event() {
		this(null, 0, null, null,null);
	}

	public Event(String title, int id, String description, String pathFile, Author author) {
		super();
		this.title = title;
		this.id = id;
		this.description = description;
		this.pathFile = pathFile;
		this.author = author;
	}
	
	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", title=" + title + ", description=" + description + ", pathFile=" + pathFile + "]";
	}	
}
