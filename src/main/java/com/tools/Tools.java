package com.tools;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Tools {

	private static final Logger logger = LogManager.getLogger(Tools.class);

	private Tools() {
		super();
	}

	public static Integer getIntFromString(HttpServletRequest request, String parameter) {

		Integer output = null;
		String index = request.getParameter(parameter);

		try {
			if (index == null) {
				return output;
			}
			if (!index.matches("^\\d+$")) {
				return output;
			}
			output = Integer.valueOf(index);
		} catch (NumberFormatException e) {
			logger.catching(e);
		}
		return output;
	}
}