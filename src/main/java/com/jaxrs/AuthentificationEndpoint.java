package com.jaxrs;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.service.EventService;


@Path("/authentication")
public class AuthentificationEndpoint {

	@Inject
	private EventService eventService;

//	public Response authenticateUser(@FormParam("username") String username, @FormParam("password") String password) {
	@POST
	@Produces("application/json")
	@Consumes("application/x-www-form-urlencoded")
	public Response authenticateUser() {

		//String username = credentials.getUsername();
	    //String password = credentials.getPassword();
		
		String username = "ninia";
		String password = "tutu";
		try {

			// Authenticate the user using the credentials provided
			authenticate(username, password);

			// Issue a token for the user
			String token = issueToken(username);

			// Return the token on the response
			System.out.println("TOOOOOOOOOOOOOOTOOOOOOOOOOOOOOOOO");
			return Response.ok(token).build();
			

		} catch (Exception e) {
			System.out.println("TIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	private void authenticate(String username, String password) throws Exception {

		boolean isAuthorRegistred = eventService.checkPassword(username, password);
		if (! isAuthorRegistred) {
			throw new Exception("Bad authentification");
		} 

	}

	private String issueToken(String username) {
		// Issue a token (can be a random String persisted to a database or a
		// JWT token)
		// The issued token must be associated to a user
		// Return the issued token
		return username;
	}
}
