package com.jaxrs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.entity.Event;
import com.service.EventService;

@Path("/")
public class EventbookResource {

	@Inject
	private EventService eventService;

	@Inject
	javax.enterprise.event.Event<Integer> sendEmailForTheEvent;

	@GET
	@Path("/events")
	public List<Event> getAllEvents() {
		return eventService.getAllEvents();
	}

	@GET
	@Path("/events/{id}")
	public Event getEventById(@PathParam("id") Integer id) {
		return eventService.getEvent(id);
	}

	@POST
	@Path("/events")
	public void postEvent(Event event) {
		eventService.createEvent(event);
	}

	@DELETE
	@Path("/events/{id}")
	public void deleteEvent(@PathParam("id") Integer id) {
		eventService.delete(id);
	}

	@GET
	@Path("/events/participants/{id}")
	public Map<String, List<String>> get(@PathParam("id") Integer id) {
		Map<String, List<String>> mapUserStatus = eventService.getUsersWithStatusParticipatingToEvent(id);
		return mapUserStatus;
	}

	@POST
	@Path("/events/participants")
	public void inviteUsers(@FormParam("id") Integer id, @FormParam("emails") String email) {
		List<String> emailToSend = new ArrayList<>();
		String[] emails = email.split(";");

		for (String string : emails) {
			emailToSend.add(string);
		}
		int pendingInvitations = eventService.insertToDbUsersAndEvents(emailToSend, id);

		if (pendingInvitations != 0) {
			sendEmailForTheEvent.fire(id);
		}
	}

	@GET
	@Path("/events/{id}/stats")
	public Map<String, Double> getStats(@PathParam("id") Integer id) {
		Map<String, Double> mapUserStats = new HashMap<String, Double>();

		mapUserStats.put("pending", eventService.calculMeanStatusForEvent(id, "pending"));
		mapUserStats.put("confirm", eventService.calculMeanStatusForEvent(id, "confirm"));
		mapUserStats.put("refuse", eventService.calculMeanStatusForEvent(id, "refuse"));
		return mapUserStats;
	}

	@PUT
	@Secured
	@Path("/events/confirm")
	public String updateStatus(JsonObject object) {
		String status = object.getString("status");
		String hashcode = object.getString("hashcode");
		if (status.equals("confirm") || status.equals("refuse")) {
			return eventService.updateUserPresenceStatus(hashcode, status, true);
		} else {
			return null;
		}
	}

	@GET
	@Path("events/user/{id}")
	public List<Event> eventsForUser(@PathParam("id") Integer id) {
		return eventService.getEventsByUser(id);
	}

}
