package com.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class RestApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources =  new HashSet<>();
		resources.add(HelloWorldResource.class);
		resources.add(SakilaResource.class);
		resources.add(EventbookResource.class);
		resources.add(AuthentificationEndpoint.class);
		//resources.add(AuthenticationFilter.class);
		return resources;
		
	}
	
}
