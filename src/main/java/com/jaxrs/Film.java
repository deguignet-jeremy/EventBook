package com.jaxrs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="film")
public class Film {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="film_id")
	private Long id;
	
	private String title;
	
	@Column(name= "language_id")
	private Long languageId;
	
	public Long getId() {
		return id;
	}
	
	public Long getLanguageId() {
		return languageId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
}
