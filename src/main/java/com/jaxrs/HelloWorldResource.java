package com.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/helloworld")
public class HelloWorldResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getHelloJSON(){
		return "[\"Hello world resource JSON !\"]";
	}

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getHelloHTML(){
		return "<p> Hello world resource HTML ! <p/>";
	}
	
	
}
