package com.jsf;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.entity.Event;
import com.service.EventService;

@Named
@ApplicationScoped
public class EventListBean {

	@Inject
	private EventService eventService;

	
	public List<Event> getEvents() {
		return eventService.getAllEvents();
	}
	
	
}
