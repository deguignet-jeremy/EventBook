package com.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.validator.constraints.NotBlank;

import com.entity.Event;
import com.service.EventService;
import com.tools.UserStatusForEvent;

@Named
@ApplicationScoped
public class EventDetailBean {

	@Inject
	private EventService eventService;

	@Inject
	javax.enterprise.event.Event<Integer> sendEmailForTheEvent;
	
	
	private Event event;
	private String username;
	private int eventId;

	private Map<String, List<String>> mapUserStatus;
	private List<String> pendingUsers;
	private List<String> confirmUsers;
	private List<String> refuseUsers;

	@NotBlank(message="Email address format is recquired")
	private String email = null;

	public void invite() {
		List<String> emailToSend = new ArrayList<>();
		String[] emails = email.split(";");
		
		for ( String string : emails) {
			emailToSend.add(string);
		}
		int pendingInvitations = eventService.insertToDbUsersAndEvents(emailToSend, eventId);

		if (pendingInvitations != 0) {
			sendEmailForTheEvent.fire(eventId);
		}
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("eventdetail.xhtml?id="+eventId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void fillUserParticipatingLists() {
		boolean isUsernameMatchWithEventAuthor = eventService.checkUsernameWithEventAuthor(username, eventId);
		if (!isUsernameMatchWithEventAuthor) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("logging.xhtml");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.mapUserStatus = eventService.getUsersWithStatusParticipatingToEvent(eventId);
		setPendingUsers(mapUserStatus.get(UserStatusForEvent.PENDING.toString().toLowerCase()));
		setConfirmUsers(mapUserStatus.get(UserStatusForEvent.CONFIRM.toString().toLowerCase()));
		setRefuseUsers(mapUserStatus.get(UserStatusForEvent.REFUSE.toString().toLowerCase()));
	}
	
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername() {
		this.username = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("username").toString();
	}

	public List<String> getPendingUsers() {
		return pendingUsers;
	}

	private void setPendingUsers(List<String> pendingUsers) {
		this.pendingUsers = pendingUsers;
	}

	public List<String> getConfirmUsers() {
		return confirmUsers;
	}

	private void setConfirmUsers(List<String> confirmUsers) {
		this.confirmUsers = confirmUsers;
	}

	public List<String> getRefuseUsers() {
		return refuseUsers;
	}

	private void setRefuseUsers(List<String> refuseUsers) {
		this.refuseUsers = refuseUsers;
	}

	public Event getEvent() {
		return event;
	}
	
	public void setEvent() {
		this.event = eventService.getEvent(eventId);;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public int getEventId() {
		return eventId;
	}
	
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
}
