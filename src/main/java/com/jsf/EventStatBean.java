package com.jsf;

import java.io.File;
import java.io.FileInputStream;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.entity.Event;
import com.service.EventService;

@Named
@RequestScoped
public class EventStatBean {
	private int eventId;
	private Event event;
	private StreamedContent chart;

	@Inject
	private EventService eventService;

	@PostConstruct
	public void init() {
		try {
			JFreeChart jfreechart = ChartFactory.createPieChart("Participant Status", createDataset(), true, true, false);
			File chartFile = new File("dynamichart");
			ChartUtilities.saveChartAsPNG(chartFile, jfreechart, 375, 300);
			chart = new DefaultStreamedContent(new FileInputStream(chartFile), "image/png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PieDataset createDataset() {
		DefaultPieDataset  pieDataset = new DefaultPieDataset();
		setEventId(Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id").toString()));
		System.out.println("EVENT ID = "+eventId);
		pieDataset.setValue("Pending", eventService.calculMeanStatusForEvent(eventId, "pending"));
		pieDataset.setValue("Confirmed", eventService.calculMeanStatusForEvent(eventId, "confirm"));
		pieDataset.setValue("Refused", eventService.calculMeanStatusForEvent(eventId, "refuse"));
		return pieDataset;
	}
	

    public StreamedContent getChart() {
        return chart;
    }
    
       
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent() {
		this.event = eventService.getEvent(eventId);
		;
	}
}
