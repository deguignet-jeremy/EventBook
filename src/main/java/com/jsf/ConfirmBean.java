package com.jsf;

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.entity.Event;
import com.service.EventService;

@Named
@RequestScoped
public class ConfirmBean {

	@NotNull
	private String status;
	private String hashCode;
	
	private Event event;

	@Inject
	private EventService eventService;

	public void confirm() {
		eventService.updateUserPresenceStatus(hashCode, status);
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("eventlist.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent() {
		this.event = eventService.getEventFromHashcode(hashCode);
	}

}
