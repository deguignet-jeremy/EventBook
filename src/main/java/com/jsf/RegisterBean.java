package com.jsf;

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.validator.constraints.NotBlank;

import com.service.EventService;

@Named
@RequestScoped
public class RegisterBean {

	@NotBlank(message="Username must be not null")
	private String username;
	@NotBlank(message="Password must be not null")
	private String passwordFirst;
	@NotBlank(message="Password must be not null")
	private String passwordSecond;

	@Inject
	private EventService eventService;

	public void register() throws IOException {
		eventService.createAuthor(username, passwordFirst);
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("logging.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordFirst() {
		return passwordFirst;
	}

	public void setPasswordFirst(String passwordFirst) {
		this.passwordFirst = passwordFirst;
	}

	public String getPasswordSecond() {
		return passwordSecond;
	}

	public void setPasswordSecond(String passwordSecond) {
		this.passwordSecond = passwordSecond;
	}

}
