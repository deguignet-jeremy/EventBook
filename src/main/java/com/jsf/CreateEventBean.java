package com.jsf;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.validator.constraints.NotBlank;

import com.entity.Event;
import com.service.EventService;

@Named
@SessionScoped
public class CreateEventBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank(message="title must be not null")
	private String title;
	
	@NotBlank(message="description must be not null")
	private String description;

	private String pathFile = null;

	private String username;

	@Inject
	private EventService eventService;

	public String create() {

		Event event = new Event();
		event.setTitle(getTitle());
		event.setDescription(getDescription());
		event.setPathFile(getPathFile());
		eventService.createEvent(pathFile, getUsername(), event);
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("eventlist.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername() {
		this.username = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("username").toString();
	}

}
