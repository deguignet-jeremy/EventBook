package com.jsf;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.hibernate.validator.constraints.NotBlank;

import com.service.EventService;

@Named
@RequestScoped
public class LoggingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message="Password must be not null")
	private String password;
	@NotBlank(message="Username must be not null")
	private String username;

	@Inject
	private EventService eventService;

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String validate() {
		boolean isExist = eventService.isAuthorExists(username);
		if (!isExist) {
			return null;
		}
		boolean isCorrect = eventService.checkPassword(username, password);
		if (!isCorrect) {
			return null;
		}
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute("username", username);

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("eventlist.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
