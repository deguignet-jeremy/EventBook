package com.jsf;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.PieChartModel;

import com.entity.Event;
import com.service.EventService;

public class ChartBean {

	private PieChartModel pieModel1;
	
	private int eventId;
	private Event event;
	private StreamedContent chart;

	@Inject
	private EventService eventService;

	
	@PostConstruct
	public void init() {
		createPieModel();
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}


	private void createPieModel() {
		pieModel1 = new PieChartModel();

		pieModel1.set("Brand 1", 540);
		pieModel1.set("Brand 2", 325);
		pieModel1.set("Brand 3", 702);
		pieModel1.set("Brand 4", 421);

		pieModel1.setTitle("Simple Pie");
		pieModel1.setLegendPosition("w");
	}

}
