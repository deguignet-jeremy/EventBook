package com.jsf.demo;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class QueryBean {

	private Integer id;

	public void setId(Integer id) { //phase Update model values - 3
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

		
	public String load(){ // phase Invoke application - 5
		// effectuer traitement requis, this.id est dispo
		// et correspond a ce qui etait ds le id de la queryString
		
		return "hello-jsf"; //redirection sur hello-jsf
	}
	
//	public void load(){ // phase Invoke application - 5
//		// effectuer traitement requis, this.id est dispo
//		// et correspond a ce qui etait ds le id de la queryString
//		
//		FacesContext.getCurrentInstance().getExternalContext().redirect("url");
//	}
	
	
}
