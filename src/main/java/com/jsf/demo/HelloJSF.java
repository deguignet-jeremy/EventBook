package com.jsf.demo;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class HelloJSF {
	
	private Integer random;
	
	public String getHello(){
		return "hello jsf :-)";
	}
	
	public void random(){
	
	}

}
