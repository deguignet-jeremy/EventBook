package com.jsf.demo;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.jboss.logging.Logger;

@Named
@RequestScoped
public class ListBean {
	
	private static final Logger LOGGER = Logger.getLogger(ListBean.class.getName());

	public List<Integer> getIntegers(){
		LOGGER.info("called getIntegers()");
		return Arrays.asList(1,2,3,4,5,6);
	}
	
}
