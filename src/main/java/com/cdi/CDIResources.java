package com.cdi;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.mail.Session;
import javax.sql.DataSource;

@ApplicationScoped
public class CDIResources {

		@Produces @EventDatabase @Resource(lookup = "java:jboss/DataSources/eventDS")
		private DataSource eventDS;
		
		@Produces @Resource(name = "java:jboss/mail/Gmail")
		private Session session;

	

}
