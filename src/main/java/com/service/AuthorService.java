package com.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.entity.Author;
import com.entity.Event;

@Stateless
public class AuthorService {

	@PersistenceContext(unitName = "eventbook")
	private EntityManager em;
	
	public Event findAuthorById(int id){
		//return soit entite soir null
		return em.find(Event.class,	id);
	}
	
	public List<Event> findEventsByAuthor(Author author){
		String jpql = "SELECT r FROM Events r WHERE author_id = :author";
		TypedQuery<Event> query = em.createQuery(jpql, Event.class);
		return query.setParameter("author", author.getId()).getResultList();
		
	}
	
}
