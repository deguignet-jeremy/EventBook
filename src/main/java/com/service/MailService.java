package com.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entity.Event;
import com.entity.User;

/**
 * Servlet implementation class MailServlet
 */
@Stateless
public class MailService extends HttpServlet {

	private static final long serialVersionUID = 1L;


	private static final Logger logger = LogManager.getLogger(MailService.class);
	
	@Inject
	private Session session;

	@Inject
	private EventService eventService;

	public void sendMail(@Observes Integer eventId) {

		try {
			Event event = eventService.getEvent(eventId);
			List<User> users = eventService.getPendingUsersFromAnEvent(eventId);
			for (User user : users) {
				sendEmailToUser(user, event);
			}
		} catch (IllegalStateException | MessagingException | EventServiceException e) {
			logger.catching(e);
			throw new MailServiceException("Can not send email to participant", e);
		}
	}

	private void sendEmailToUser(User user, Event event) throws MessagingException {

		try {
		String hashCode = eventService.getHashCodeFromDB(event.getId(), user.getId());
//		String url = "http://localhost:8080/eventbook/ConfirmServlet?id=";
		String url = "http://localhost:8080/eventbook/faces/xhtml/confirm.xhtml?id=";
		MimeMessage message = new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
			message.setSubject("Hello");
			String htmlMessage = "<div style=\"color:red;\"><a href=" + url + hashCode + ">" + event.getTitle()
					+ "</a>	</div>";
			message.setContent(htmlMessage, "text/html; charset=utf-8");
			Transport.send(message);
		} catch (MessagingException | EventServiceException e) {
			logger.catching(e);
			throw e;
		}
	}
}
