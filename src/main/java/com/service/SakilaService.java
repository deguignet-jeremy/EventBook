package com.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.jaxrs.Film;

@Stateless
public class SakilaService {

	@PersistenceContext(unitName="sakila")
	private EntityManager em;
	
	public Film findById(Long id){
		return em.find(Film.class, id);
	}
		
	public List<Film> findAll(int offset, int numberOfFilms){
		TypedQuery<Film> query = em.createQuery("FROM Film f", Film.class);
		query.setFirstResult(offset);
		query.setMaxResults(numberOfFilms);
		return query.getResultList();
	}

	public void create(Film film) {
		em.persist(film);
		
	}

	public void updateFilm(Film film) {
		Film updatedFilm = em.merge(film);
		
	}

	public void delete(Long id) {
		Film film = em.find(Film.class, id);
		em.remove(film);
		
	}
	
	
	
}
