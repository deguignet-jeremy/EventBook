package com.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import com.entity.Author;
import com.entity.Event;
import com.entity.User;
import com.entity.UserParticipatingToEvent;
import com.tools.HashGen;
import com.tools.UserStatusForEvent;

@Stateless
public class EventService {
	private static final Logger logger = LogManager.getLogger(EventService.class);
	private static final String DESCRIPTION = "description";
	private static final String USERNAME = "username";
	private static final String EVENTID = "eventId";
	private static final String USERID = "userId";

	@PersistenceContext(unitName = "eventbook")
	private EntityManager em;

	public void putHashCode(String hashCode, int userId, int eventId) {

		String jpql = "SELECT u FROM UserParticipatingToEvent u WHERE u.user.id=:userId AND u.event.id=:eventId";
		TypedQuery<UserParticipatingToEvent> query = em.createQuery(jpql, UserParticipatingToEvent.class);
		query.setParameter(USERID, userId);
		query.setParameter(EVENTID, eventId);
		UserParticipatingToEvent u = query.getSingleResult();
		u.setHashCode(hashCode);
	}

	/**
	 * 
	 * @param eventId
	 * @return Event
	 */
	public Event getEvent(int eventId) {
		return em.find(Event.class, eventId);
	}

	public User getUser(int userId) {
		return em.find(User.class, userId);
	}

	public User insertNewUser(String email) {

		User user = new User();
		user.setEmail(email);
		em.persist(user);
		return user;
	}

	public User findUserByEmail(String email) {

		String jpql = "SELECT user FROM User user WHERE user.email=:email";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		query.setParameter("email", email);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			logger.catching(e);
			return null;
		}
	}

	public void insertUserParticipateToEvent(int eventId, int userId) {

		UserParticipatingToEvent u = new UserParticipatingToEvent();
		Event event = getEvent(eventId);
		User user = getUser(userId);
		u.setEvent(event);
		u.setUser(user);
		u.setStatus(UserStatusForEvent.PENDING.toString().toLowerCase());
		em.persist(u);
	}

	public Map<String, List<String>> getUsersWithStatusParticipatingToEvent(int eventId) {

		List<UserParticipatingToEvent> userParticipatingToEvents = new ArrayList<>();
		String jpql = "SELECT u FROM UserParticipatingToEvent u WHERE u.event.id=:eventId";
		TypedQuery<UserParticipatingToEvent> query = em.createQuery(jpql, UserParticipatingToEvent.class);
		query.setParameter(EVENTID, eventId);
		userParticipatingToEvents.addAll(query.getResultList());

		Map<String, List<String>> map = new HashMap<>();
		map.put(UserStatusForEvent.PENDING.toString().toLowerCase(), new ArrayList<>());
		map.put(UserStatusForEvent.CONFIRM.toString().toLowerCase(), new ArrayList<>());
		map.put(UserStatusForEvent.REFUSE.toString().toLowerCase(), new ArrayList<>());

		for (UserParticipatingToEvent user : userParticipatingToEvents) {
			List<String> list = map.get(user.getStatus());
			String email = user.getUser().getEmail();
			if (!list.contains(email)) {
				list.add(email);
			}
			map.put(user.getStatus(), list);
		}
		return map;
	}

		//servlet
	public void createEvent(String pathFile, HttpServletRequest request) {

		String username = request.getSession(false).getAttribute(USERNAME).toString();
		String jpql = "SELECT author FROM Author author WHERE author.username=:username";
		TypedQuery<Author> query = em.createQuery(jpql, Author.class);
		query.setParameter(USERNAME, username);

		Event event = new Event();
		event.setTitle(request.getParameter("title"));
		event.setDescription(request.getParameter(DESCRIPTION));
		event.setPathFile(pathFile);
		try {
			event.setAuthor(query.getSingleResult());
			em.persist(event);
		} catch (NoResultException e ) {
			logger.catching(e);
			throw new EventServiceException("Can not create the event", e);
		}

	}
	
	//jsf
	public void createEvent(String pathFile,String username, Event event) {

		String jpql = "SELECT author FROM Author author WHERE author.username=:username";
		TypedQuery<Author> query = em.createQuery(jpql, Author.class);
		query.setParameter(USERNAME, username);
		logger.info("username TOTO "+username);


		try {
			event.setAuthor(query.getSingleResult());
			em.persist(event);
		} catch (NoResultException e ) {
			logger.catching(e);
			throw new EventServiceException("Can not create the event", e);
		}

	}
	
	public void createEvent(Event event) {

		try {
			em.persist(event);
		} catch (NoResultException e ) {
			logger.catching(e);
			throw new EventServiceException("Can not create the event", e);
		}

	}

	public List<Event> getAllEvents() {

		String jpql = "SELECT event FROM Event event";
		TypedQuery<Event> query = em.createQuery(jpql, Event.class);
		return query.getResultList();
	}

	public List<User> getPendingUsersFromAnEvent(int eventId) {

		String jpql = "SELECT u.user FROM UserParticipatingToEvent u WHERE u.status='pending' AND u.event.id=:eventId";
		TypedQuery<User> query = em.createQuery(jpql, User.class);
		query.setParameter(EVENTID, eventId);
		return query.getResultList();
	}

	public String getHashCodeFromDB(int eventId, int userId) {

		String hashCode = null;
		String jpql = "SELECT u.hashCode FROM UserParticipatingToEvent u WHERE u.user.id=:userId AND u.event.id=:eventId";
		TypedQuery<String> query = em.createQuery(jpql, String.class);
		query.setParameter(EVENTID, eventId);
		query.setParameter(USERID, userId);

		try {
			hashCode = query.getSingleResult();
		} catch (NoResultException e) {
			logger.catching(e);
			throw new EventServiceException("Can not get hashcode in the database", e);
		}
		return hashCode;
	}

	public void updateUserPresenceStatus(String hashcode, String status) {

		String jpql = "SELECT u FROM UserParticipatingToEvent u WHERE u.hashCode=:hashcode";
		TypedQuery<UserParticipatingToEvent> query = em.createQuery(jpql, UserParticipatingToEvent.class);
		query.setParameter("hashcode", hashcode);
		UserParticipatingToEvent u = query.getSingleResult();
		u.setStatus(status);
	}
	
	public String updateUserPresenceStatus(String hashcode, String status, Boolean key) {

		String jpql = "SELECT u FROM UserParticipatingToEvent u WHERE u.hashCode=:hashcode";
		TypedQuery<UserParticipatingToEvent> query = em.createQuery(jpql, UserParticipatingToEvent.class);
		query.setParameter("hashcode", hashcode);
		UserParticipatingToEvent u = query.getSingleResult();
		u.setStatus(status);
		return u.getStatus();
	}

	public Event getEventFromHashcode(String hashcode) {

		Event event = new Event();
		String jpql = "SELECT u.event FROM UserParticipatingToEvent u WHERE u.hashCode=:hashcode";
		TypedQuery<Event> query = em.createQuery(jpql, Event.class);
		query.setParameter("hashcode", hashcode);

		try {
			event = query.getSingleResult();
		} catch (Exception e) {
			logger.catching(e);
			throw new EventServiceException("Can not get event from hashcode in the database", e);
		}
		return event;
	}

	public double calculMeanStatusForEvent(int eventId, String status) {
		String jpql = "SELECT (SELECT count(u.status) FROM UserParticipatingToEvent u WHERE u.status=:status AND u.event.id=:eventId)*1.00/ count(*) FROM UserParticipatingToEvent u WHERE u.event.id=:eventId ";
		TypedQuery<Double> query = em.createQuery(jpql, Double.class);
		query.setParameter("status", status);
		query.setParameter(EVENTID, eventId);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			logger.catching(e);
			throw new EventServiceException("Can not calcul the mean status for the event in the database", e);
		}
	}

	public boolean checkUserAlreadyParticipatingToEvent(int eventId, int userId) {

		String jpql = "SELECT count(*) FROM UserParticipatingToEvent u WHERE u.event.id=:eventId AND u.user.id=:userId";
		TypedQuery<Long> query = em.createQuery(jpql, Long.class);
		query.setParameter(EVENTID, eventId);
		query.setParameter(USERID, userId);
		return query.getSingleResult() != 0;
	}

	public int insertToDbUsersAndEvents(List<String> emailToSend, Integer eventId) {

		int addedInDb = 0;
		for (String mail : emailToSend) {
			int userId = findUserByEmail(mail).getId();
			if (userId == 0) {
				userId = insertNewUser(mail).getId();
			}

			boolean isExist = checkUserAlreadyParticipatingToEvent(eventId, userId);
			if (!isExist) {
				insertUserParticipateToEvent(eventId, userId);
				String idConcat = mail + '#' + eventId;
				String hashCode = HashGen.encryptEmailUserIdEvent(idConcat);
				putHashCode(hashCode, userId, eventId);
				addedInDb++;
			}
		}
		return addedInDb;
	}

	public boolean isAuthorExists(String username) {

		String jpql = "SELECT COUNT(author) FROM Author author WHERE author.username=:username";
		TypedQuery<Long> query = em.createQuery(jpql, Long.class);
		query.setParameter(USERNAME, username);

		return query.getSingleResult() != 0 ? true : false;
	}

	/**
	 * check if the author is registred in the DB
	 * @param username
	 * @param password
	 * @return boolean is author registred 
	 */
	public boolean checkPassword(String username, String password) {

		String jpql = "SELECT author FROM Author author WHERE author.username=:username";
		TypedQuery<Author> query = em.createQuery(jpql, Author.class);
		query.setParameter(USERNAME, username);
		Author author = query.getSingleResult();

		return BCrypt.checkpw(password, author.getPassword());
	}

	public boolean checkUsernameWithEventAuthor(String username, int eventId) {

		String jpql = "SELECT author FROM Author author WHERE author= (SELECT event.author FROM Event event WHERE event.id =:eventId)";
		TypedQuery<Author> query = em.createQuery(jpql, Author.class);
		query.setParameter(EVENTID, eventId);
		Author author = query.getSingleResult();

		return username.equals(author.getUsername()) ? true : false;
	}

	//servlet
	public void createAuthor(HttpServletRequest request) {

		String encryptPassword = HashGen.encryptPassword(request.getParameter("password"));

		Author author = new Author();
		author.setUsername(request.getParameter(USERNAME));
		author.setPassword(encryptPassword);
		em.persist(author);
	}
	
	//jsf
	public void createAuthor(String username, String password) {

		String encryptPassword = HashGen.encryptPassword(password);

		Author author = new Author();
		author.setUsername(username);
		author.setPassword(encryptPassword);
		em.persist(author);
	}

	public void delete(Integer id) {
		Event event = em.find(Event.class, id);
		em.remove(event);
	}

	public List<Event> getEventsByUser(Integer id) {
		
		List<Event> events = new ArrayList<>();
		String jpql = "SELECT u.event FROM UserParticipatingToEvent u WHERE u.user.id=:id";
		TypedQuery<Event> query = em.createQuery(jpql, Event.class);
		query.setParameter("id", id);

		try {
			events = query.getResultList();
		} catch (Exception e) {
			logger.catching(e);
			throw new EventServiceException("Can not get events from user id in the database", e);
		}		
		return events;
	}
}
