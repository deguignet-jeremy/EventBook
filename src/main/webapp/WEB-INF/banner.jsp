<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<body>

	<c:if test="${(empty sessionScope.username)}">
		<span><a href="/eventbook/Login">Login</a></span>
		<span><a href="/eventbook/RegisterServlet">Register</a></span>
	</c:if>
	<c:if test="${(not empty sessionScope.username)}">
		<span> ${sessionScope.username}</span>
		<span><a href="/eventbook/Logout">Logout</a></span>
	</c:if>
	
	<div>
		<a href="/eventbook/EventListServlet">Home</a>
	</div>

</body>