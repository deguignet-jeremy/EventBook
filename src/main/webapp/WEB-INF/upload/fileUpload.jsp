<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>File upload</title>
</head>
<body>

	<form method="POST" enctype="multipart/form-data">
		<input type="text" name="title" /> 
		<input type="file" name="file" />
		<input type="submit" value="upload" />
	</form>
</body>
</html>