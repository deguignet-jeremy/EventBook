<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Create event</title>
</head>
<body>

	<form action="CreateEventServlet" method="POST"
		enctype="multipart/form-data">
		<div>
			<label for="title">Event name</label> <input id="title" type="text"
				name="title" value="${ title }" />
			<c:if test="${(not empty errors) && (not empty errors['title'])}">
				<span>${ errors['title'] }</span>
			</c:if>
		</div>
		<div>
			<label for="description">Description</label>
			<textarea name="description">${ description }</textarea>
			<c:if
				test="${(not empty errors) && (not empty errors['description'])}">
				<span>${ errors['description'] }</span>
			</c:if>

		</div>

		<div>
			<input type="file" name="file" /> 
		</div>

		<div>
			<input type="submit" value="Create your event !" />
		</div>

	</form>

</body>
</html>