<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Create event</title>
</head>
<body>

	<form action="Login" method="POST">
		<div>
			<label for="username">Name</label> 
			<input id="username" type="text" name="username" value="${username}"/>
		</div>
		<div>
			<label for="password">Password</label>
			<input id="password" type="password" name="password" value="${password}"/>
		</div>
		<div>
			<input type="submit" value="Log In" />
		</div>
	</form>
</body>
</html>