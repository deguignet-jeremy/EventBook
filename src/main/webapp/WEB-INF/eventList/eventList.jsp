<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Welcome !</title>
</head>
<body>

	<div id="menu">
		<ul id="onglets">
			<li><a href="index.html"> Accueil </a></li>
			<li class="active"><a href="EventListServlet"> All Events </a></li>
		</ul>
	</div>

	<div class="createEvent">
		<form action="/eventbook/CreateEventServlet">
			<input type="submit" value="Create a new event" />
		</form>
	</div>

	<div class=listEvents>
		<ul>
			<c:forEach var="event" items="${events}">
				<li><div class="list__event">
					<a href="EventDetailServlet?id=${event.id}">${event.title}</a>			
				</div>
				</li>
			</c:forEach>
		</ul>
	</div>
	
	<div class=returnHome>
		<a href="/eventbook/EventListServlet">Back</a>
	</div>

</body>
</html>