<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>${event.title}</title>
</head>
<body>
	<div id="menu">
		<ul id="onglets">
			<li><a href="index.html"> Accueil </a></li>
			<li class="active"><a href="EventDetailServlet?id=${event.id}">${event.title}
					status</a></li>
			<li><a href="EventStatServlet?id=${event.id}">${event.title}
					statistics</a></li>
		</ul>
	</div>


	<div>
		<h1>${event.title}</h1>
		<img class="eventImage" src="/eventbook/ImageServlet?id=${event.id}">
	</div>
	

	<form action="EventDetailServlet" method="POST">
		<div>
			<label for="email">Invite new participants :</label>
			<textarea id="emailList" name="email"></textarea>
			<c:if test="${(not empty errors) && (not empty errors['email'])}">
				<span>${ errors['email'] }</span>
			</c:if>
		</div>
		<input type="hidden" name="eventId" value="${eventId}"> <input
			type="hidden" name="urlMail" value="MailServlet?id=${event.id}">

		<div>
			<input type="submit" value="INVITE" />
		</div>
	</form>
	<div class="listePending">
		<label>Pending users:</label>
		<ul>
			<c:forEach var="mail" items="${pendingUsers}">
				<li>${mail}</li>
			</c:forEach>
		</ul>
	</div>
	<div class="listeConfirmed">
		<label>Confirmed users:</label>
		<ul>
			<c:forEach var="mail" items="${confirmUsers}">
				<li>${mail}</li>
			</c:forEach>
		</ul>
	</div>
	<div class="listeRefused">
		<label>Refused users:</label>
		<ul>
			<c:forEach var="mail" items="${refuseUsers}">
				<li>${mail}</li>
			</c:forEach>
		</ul>
	</div>




	<div class=returnHome>
		<a href="/eventbook/EventListServlet">Back</a>
	</div>

</body>
</html>