<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Statistics</title>
</head>
<body>
	<div id="menu">
		<ul id="onglets">
			<li><a href="index.html"> Accueil </a></li>
			<li><a href="EventDetailServlet?id=${event.id}">${event.title}
					status</a></li>
			<li class="active"><a href="EventStatServlet?id=${event.id}">${event.title}
					statistics</a></li>
		</ul>
	</div>
	

		<H1>Participation to ${event.title}</h1>
		<img src="/eventbook/DisplayPieServlet?id=${event.id}">


	<div class=returnHome>
		<a href="/eventbook/EventListServlet">Back</a>
	</div>

</body>
</html>