<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp"/>
<jsp:include page="../flashBanner.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Confirm your presence</title>
</head>
<body>

	<h1>${event.title}</h1>

	<div>
		<p>${event.description}</p>
	</div>

	<form action="ConfirmServlet" method="POST">

		<fieldset>
			<legend> Presence? </legend>
			<p>
				<input type="submit" name="eventPresence" value="confirm" /> <input type="submit" name="eventPresence"
					value="refuse" />
			</p>
		</fieldset>
	<input type="hidden" name="hashCode" value="${hashCode}">
	</form>
	
		<div class=returnHome>
		<a href="/eventbook/index.html">Back</a>
		</div>

</body>
</html>