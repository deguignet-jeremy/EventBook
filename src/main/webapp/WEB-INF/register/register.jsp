<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../banner.jsp" />
<jsp:include page="../flashBanner.jsp" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/eventbook.css" />
<title>Register</title>
</head>
<body>

	<form action="RegisterServlet" method="POST">
		<div>
			<label for="username">Username</label> <input id="username"
				type="text" name="username" />
			<c:if test="${(not empty errors) && (not empty errors['username'])}">
				<span>${ errors['username'] }</span>
			</c:if>
		</div>
		<div>
			<label for="password">Password</label> <input id="password"
				type="password" name="password" />
			<c:if
				test="${(not empty errors) && (not empty errors['passwordFirst'])}">
				<span>${ errors['passwordFirst'] }</span>
			</c:if>
		</div>
		<div>
			<label for="confirm password">Confirm password</label> <input
				id="confirm password" type="password" name="confirm password" />
			<c:if
				test="${(not empty errors) && (not empty errors['passwordSecond'])}">
				<span>${ errors['passwordSecond'] }</span>
			</c:if>
			<c:if
				test="${(not empty errors) && (not empty errors['checkPassword'])}">
				<span>${ errors['checkPassword'] }</span>
			</c:if>
		</div>


		<div>
			<input type="submit" value="Register" />
		</div>

	</form>

</body>
</html>